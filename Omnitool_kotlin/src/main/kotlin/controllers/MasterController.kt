package controllers

import fragments.NotImplementedFragment
import fragments.PopupFragment
import javafx.stage.StageStyle
import tornadofx.Controller
import views.StatusBar
import views.MasterView

class MasterController: Controller() {
    
    val mv: MasterView by inject()
    val statusBar: StatusBar by inject()
    
    
    fun writeToDb(inputValue: String) {
        println("Writing $inputValue to database!")
    }
    
    fun notImplementedPopup() {
        find<NotImplementedFragment>().openWindow(stageStyle = StageStyle.UTILITY)
    }
    
    /**
     * This method creates a popup with a title
     *  
     * @param popuptitle
     * @param popupMessage
     */
    fun popup(popuptitle: String, popupMessage: String) {
        find<PopupFragment>(){
            message.value = popupMessage
            title = popuptitle
        }.openModal(
            stageStyle = StageStyle.UTILITY
        )
    }
    
    /**
     * This method creates a popup with just body text
     */
    fun popup(popupMessage: String) {
        find<PopupFragment>(){
            message.value = popupMessage
        }.openModal(
            stageStyle = StageStyle.UTILITY
        )
    }
    
//    fun updateStatusBarMessage(sbUpdate: String)
//    {
//        var textMessage = "<no text>"
//        if(sbUpdate != "")
//        {
//            textMessage = sbUpdate
//        }
//        println("Updating status bar text to: " + textMessage)
//        statusBar.statusMessage.value = textMessage
//    }
    
    
    
    
}//end controller
