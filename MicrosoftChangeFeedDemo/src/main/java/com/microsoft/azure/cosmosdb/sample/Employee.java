package com.microsoft.azure.cosmosdb.sample;

public class Employee
{
	int employeeNumber;
	EmployeeType employeeType;
	
	public Employee(int employeeNumber, EmployeeType employeeType) {
		super();
		this.employeeNumber = employeeNumber;
		this.employeeType = employeeType;
	}
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public EmployeeType getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}
	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + ", employeeType=" + employeeType + "]";
	}
	
	
}