package com.avro.mappers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.avro.AvroExtractor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


class AvroSchemaFieldTypeRecord
{
	public String fieldName;
	
	public String supertypeFields;

	/**
	 * @param typeName
	 * @param fields
	 */
	public AvroSchemaFieldTypeRecord(String fieldName, String supertypeFields) {
		super();
		this.fieldName = fieldName;
		this.supertypeFields = supertypeFields;
		
		AvroExtractor.debug("\n\nNEW " + this.toString() + "\n");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AvroSchemaFieldTypeRecord [fieldName=" + fieldName + ", supertypeFields=" + supertypeFields + "]";
	}
	
	
}








/******************************************************************************
 * @author afalco4g
 *
 *****************************************************************************/
@JsonInclude(Include.NON_NULL)
public class AvroSchemaFieldMapper
{
	@JsonIgnore
	private LinkedList<AvroSchemaFieldTypeRecord> recordTypeInfo = new LinkedList<AvroSchemaFieldTypeRecord>();
	
	
	private String name         = "";
	private String type         = "";
	private String doc          = "";
	private String logicalType  = "";
	
	@JsonProperty("default")
	private String defaultValue = "";
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	
	
	
	
	/**
	 * @param type the type to set
	 */
	@SuppressWarnings("unchecked")
	public void setType(Object rootTypeField) 
	{
		this.type = processType(rootTypeField);
//		this.type = "";
		
		
		
		
		
//		if(rootTypeField instanceof ArrayList)
//		{
//			this.type = getInnerType_arrayList(rootTypeField);
//		}
//		
//		else if(rootTypeField instanceof LinkedHashMap)
//		{
//			this.type = getInnerType_linkedHashMap(rootTypeField);
//		}
//		
//		else
//		{
//			//This is a flat string
//			this.type += (String)rootTypeField;
//		}		
//		
//		AvroExtractor.debug("Type string is set: " + this.type);
//		
//		//*********************************************************************
//		//Final string formats if present
//
//		this.type = this.type.replaceAll(" ", "");
//		this.type = this.type.replaceAll(",", ";");
//		
//		if(this.type.endsWith(";")) 
//			this.type = this.type.substring(0, this.type.length()-1);
//		
//		returningStringToSetToType = "";
		
		System.out.println("blah");
	}
	
	
	
	
	
	
	
	
	
	/**
	 * @return the doc
	 */
	public String getDoc() {
		return doc;
	}
	/**
	 * @param doc the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}
	
	/**
	 * @return the logicalType
	 */
	public String getLogicalType() {
		return logicalType;
	}
	/**
	 * @param logicalType the logicalType to set
	 */
	public void setLogicalType(String logicalType) {
		this.logicalType = logicalType;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AvroSchemaFieldMapper [name=" + name + ", type=" + type + ", doc=" + doc + ", logicalType="
				+ logicalType + ", defaultValue=" + defaultValue + "]";
	}
	
	
	
//	@JsonProperty("type")
//    private void unpackNested(ArrayNode type) {
//		ObjectMapper om = new ObjectMapper();
//		TypeFactory typeFactory = om.getTypeFactory();
//		try {
//			List<String> list = om.readValue(type.toString(), typeFactory.constructCollectionType(List.class, String.class));
//			for(String x : list)
//			{
//				this.type += x;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    }
	
	/**
	 * @return
	 */
	public String toCsvFormat()
	{
		String returnString = "";
		
		returnString += name;
		returnString += ",";
		returnString += type;
		returnString += ",";
		returnString += doc;
		returnString += ",";
//		returnString += logicalType;
//		returnString += ",";
		returnString += defaultValue;
		
		
		if(this.recordTypeInfo.isEmpty())
		{
			//Don't do anything else
		}
		else
		{
			//TODO
			for( AvroSchemaFieldTypeRecord subRecordType: this.recordTypeInfo)
			{
				AvroExtractor.debug(subRecordType.toString());
				String input = "\n,,"+subRecordType.fieldName + "["+subRecordType.supertypeFields + "]";
				AvroExtractor.debug(input);
				
				returnString += input;
			}
		}
		
		
		
		return returnString;
	}
	
	public String processRecordCsvLine(String type)
	{
		AvroExtractor.debug("toCsvFormat: type is record.");
		String manipulatedString = "";
		manipulatedString = type.replaceFirst("record\\[","");
		manipulatedString = manipulatedString.replaceFirst("record\\[","[");
		
		
		
		
		AvroExtractor.debug("manipulatedString : " + manipulatedString);
//		returnString += Arrays.toString(type.split("record\\["));
		
		return null;
	}
	
	
	
	/**
	 * @param type
	 */
	@SuppressWarnings("unchecked")
	private String getInnerType_arrayList(Object type)
	{
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());
		
		String s = "";
		
		ArrayList<String> typeValues = (ArrayList<String>) type;
		
		//First remove any instances of nulls cause we dont need those, duh
		AvroExtractor.debug("Removing nulls from ArrayList<String>...");
		typeValues.removeAll(Collections.singleton("null"));
		
		AvroExtractor.debug("ArrayList: " + typeValues);
		
		/* 
		 * ArrayLists from the Jackson JSON mapper can contain either 
		 * flat strings which need to be strung together
		 * OR
		 * LinkedHashMaps which are usually specific datatypes that contain
		 * extra information
		 */
		for(Object x: typeValues)
		{
			if(x instanceof String)
			{
				AvroExtractor.debug("Adding to type string: \""+x+"\"");
				s += x +";";
			}
			else
			{
				//this method cant handle anything other than ArrayList<String>
				//recursive call the setType method
				//This will show a second debug output showing a formatted output
				
//				processType(x);
//				s = this.type;
				AvroExtractor.typeIs(x);
				s += processType(x) + ";";
			}
		}
		

		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("getInnerType_arrayList Return value: "+ s);
		return s;
	}

	/**************************************************************************
	 * @param type
	 * @return
	 **************************************************************************/
	private String getInnerType_linkedHashMap(Object type)
	{
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());

		LinkedHashMap<String,String> innerFieldType = (LinkedHashMap<String, String>) type;

		String s = "";
		
		//Found these results by trial and error since I have no way to predict
		//what type im going to get
		if((Object)innerFieldType.get("type") instanceof String)
		{

			AvroExtractor.debug("Inner field type string is: " + innerFieldType.get("type"));
			
			//Types we need to toss back up for interpretation
			
			//If we have a map type -------------------------------------------
			if(innerFieldType.get("type").equals("map"))
			{
				//inner map types contain an ArrayList<String>, toss to recursive method
				//for processing by casting as Object class
				
				s += processType(innerFieldType.get("values"));
			}
			
			
			//Direct or branching types we can deal with here
			
			//if we have a long type ------------------------------------------
			else if(innerFieldType.get("type").equals("long"))
			{
				s += innerFieldType.get("type");
				
				//Need to set the appropriate higher field from here since we are
				//inside the "inner" type
				AvroExtractor.debug("Need to set the appropriate higher field from here since we are inside the \"inner\" type");
				this.logicalType = innerFieldType.get("logicalType");
			}
			
			//if we have an enum type -----------------------------------------
			else if(innerFieldType.get("type").equals("enum"))
			{
				s += getInnerType_enum(innerFieldType);
			}
			
			//if we have a record type ----------------------------------------
			else if(innerFieldType.get("type").equals("array"))
			{
				s += getInnerType_array(innerFieldType);
			}

			//if we have a record type ----------------------------------------
			else if(innerFieldType.get("type").equals("record"))
			{
				s += getInnerType_record(innerFieldType);
			}
			
			else 
			{
				AvroExtractor.debug("Simple type assumed.");
				
				s += innerFieldType.get("name") + "[" + innerFieldType.get("type") + "]";
			}
		}
		else
		{
			//this method cant handle anything other than ArrayList<String>
			//recursive call the setType method
			
			s += processType(innerFieldType.get("type"));
		}

		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("getInnerType_linkedHashMap Return value: " + s);
		return s;
	}

	/**
	 * @param record
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getInnerType_enum(LinkedHashMap<String,String> enumField)
	{
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());
		
		String s = "";
		s = enumField.get("name");
		
		AvroExtractor.debug("Attaching symbols ArrayList as is to automatically include ArrayList toString formatting...");
		//double auto casting :(
		ArrayList<String> typeSymbols = (ArrayList<String>)  (Object)enumField.get("symbols"); 
		s += typeSymbols.toString();

		s = s.replaceAll(",", ";");
		
		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("getInnerType_enum Return value: " + s);
		return s;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * This is a super specific type returned by the Jackson JSON mapper object
	 * This method is only meant for a "record" type field (Honestly this is
	 * getting confusing the more I have to dig)<br><br>
	 * @param record
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getInnerType_record(LinkedHashMap<String,String> innerFieldType)
	{
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());
		
		String s = innerFieldType.get("name"); 
		
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//TODO 8-19-19: New format goal
		//Final format goal: 
		// field1                        fieldType
		// field1.FieldType              [FieldType, fieldsInFieldType]
		// field1.FieldType.FieldType    [FieldType, fieldsInFieldType]
		// ...                           ...
		String fieldString = getInnerType_arrayList((Object)innerFieldType.get("fields"));
		AvroSchemaFieldTypeRecord avroRecord = 
				new AvroSchemaFieldTypeRecord(innerFieldType.get("name"), 
						fieldString);
		AvroExtractor.debug(avroRecord.toString());
		//add the record object to this
		this.recordTypeInfo.add(avroRecord);
		
		
		
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		//Always return blank value for record fields because of CSV output
		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("getInnerType_record Return value: " + s);
		return s;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * This is a super specific type returned by the Jackson JSON mapper object
	 * This method is only meant for a "record" type field (Honestly this is
	 * getting confusing the more I have to dig)<br><br>
	 * @param record
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getInnerType_array(LinkedHashMap<String,String> innerFieldType)
	{
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());
		
		String s = ""; 
		//Final format goal: recordName[field1[fieldType];field2[fieldType],...]
		Object genericItems = ((Object) innerFieldType.get("items"));
		
		if(genericItems instanceof String)
		{
			AvroExtractor.debug("Processing String...");
			s += innerFieldType.get("type") + "[" + innerFieldType.get("items") + "]";
		}
		else
		{
			//this method cant handle anything other than ArrayList<String>
			//recursive call the setType method
			AvroExtractor.typeIs(genericItems);
			AvroExtractor.debug(genericItems.toString());
			s += processType(genericItems);
		}
		
		
		

		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("getInnerType_array Return value: " + s);
		return s;
	}
	
	
	
	
	
	
	
	/**
	 * @param rootTypeField
	 * @return
	 */
	private String processType(Object rootTypeField)
	{
		System.out.println("\n");
		AvroExtractor.entering(Thread.currentThread().getStackTrace()[1].getMethodName());

		String returningStringToSetToType = "";

		AvroExtractor.typeIs(rootTypeField);
		

//		//Figure out what kind of object we have for type
//		//damn avro files could have multiple different types for
//		
//		//NOTE: instanceof is dirty code, but this isn't official, so thats my
//		//excuse for Q&D code
//		//*********************************************************************
		
		if(rootTypeField instanceof String)
		{
			returningStringToSetToType += (String) rootTypeField;
		}
		else if(rootTypeField instanceof ArrayList)
		{
			returningStringToSetToType = getInnerType_arrayList(rootTypeField);
		}
		
		else if(rootTypeField instanceof LinkedHashMap)
		{
			returningStringToSetToType = getInnerType_linkedHashMap(rootTypeField);
		}
		
		

		AvroExtractor.exiting(Thread.currentThread().getStackTrace()[1].getMethodName());
		AvroExtractor.debug("processType Returning value: " + returningStringToSetToType);
		return returningStringToSetToType;
	}//end processType
	
	
	
	
	
	
}//end class




















