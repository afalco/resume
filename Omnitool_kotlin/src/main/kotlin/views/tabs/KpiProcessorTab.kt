package views.tabs

import controllers.tabs.KpiProcessorController
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.View
import tornadofx.action
import tornadofx.button
import tornadofx.field
import tornadofx.fieldset
import tornadofx.form
import tornadofx.label
import tornadofx.radiobutton
import tornadofx.selectedValueProperty
import tornadofx.textfield
import tornadofx.togglegroup
import tornadofx.vbox

class KpiProcessorTab: View("KPI Processor")  {
    
    val kpic: KpiProcessorController by inject()
    
    private val kpiName = SimpleStringProperty()
    
    private val kpiDescription = SimpleStringProperty()
    
    private val toggleGroupValue = SimpleObjectProperty<String>()
//    private val toggleGroup = ToggleGroup()
    
    
    override val root = vbox {
        label(" This will create Oracle SQL based on the KPI criteria below")
        form {
	    	fieldset {
	    		field("KPI Name:") {
	    			textfield(kpiName)
	            }
                field("KPI Description") {
                    textfield(kpiDescription)
                }
		        togglegroup() {
                    toggleGroupValue.bind(selectedValueProperty())
//                    hbox(){
		            radiobutton("TXT", value="txt"){isSelected=true}
		            radiobutton("SQL", value="sql")
		            radiobutton("CSV", value="csv")
//                    }
		        }
	            button("Submit") {
	            	action {
                        kpic.statusBar.clearStatusBarMessage();
//                        kpic.createFiles(kpiName, kpiDescription, toggleGroupValue.value)
	                }
	            }
	        }//end fieldset
        }// end form
    }//end vbox
}//end fragment
