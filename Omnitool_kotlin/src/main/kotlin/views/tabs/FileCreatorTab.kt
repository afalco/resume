package views.tabs

import controllers.tabs.FileCreatorTabController
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.control.RadioButton
import javafx.scene.control.ToggleGroup
import tornadofx.*

class FileCreatorTab: View("FileCreator")  {
    
    val fctc: FileCreatorTabController by inject()
    
    private val dirToCreateFilesIn = SimpleStringProperty()
    
    private val numOfFilesToCreate = SimpleStringProperty()
    
    private val toggleGroupValue = SimpleObjectProperty<String>()
//    private val toggleGroup = ToggleGroup()
    
    
    override val root = vbox {
        label(" This will create numbered files within a directory")
        form {
	    	fieldset {
	    		field("Directory:") {
	    			textfield(dirToCreateFilesIn)
	            }
                field("Number of Files") {
                	textfield(numOfFilesToCreate)
                }
		        togglegroup {
                    toggleGroupValue.bind(selectedValueProperty())
                    
		            radiobutton("TXT", value="txt"){isSelected=true}
		            radiobutton("SQL", value="sql")
		            radiobutton("CSV", value="csv")
		        }
	            button("Submit") {
	            	action {
                        fctc.statusBar.clearStatusBarMessage();
                        fctc.createFiles(dirToCreateFilesIn, numOfFilesToCreate, toggleGroupValue.value)
	                }
	            }
	        }//end fieldset
        }// end form
    }//end vbox
}//end fragment
