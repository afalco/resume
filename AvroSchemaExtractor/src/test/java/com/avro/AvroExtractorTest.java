package com.avro;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.avro.Schema;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.avro.mappers.AvroSchemaFieldMapper;
import com.avro.mappers.AvroSchemaMapper;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AvroExtractorTest {

	public static boolean debug = true;
	public static boolean userInput = false;
	
	static boolean fieldsOnly = true;
	static boolean schemaOnly = true;

	static ObjectMapper mapper = null;
	
	
	/**
	 * 
	 */
	@After
	public void afterTest()
	{
		mapper = null;
	}
	
	
	/**
	 * @param filename
	 * @return
	 */
	public JsonNode initialize(String filename)
	{
		File avroFile = new File(filename);
		JsonNode rootNode = null;
		
		printFileTitle(avroFile);
		
		Schema.Parser parser = new Schema.Parser();
        Schema schema = null;
        try {
            schema = parser.parse(avroFile);
        } catch (IOException e) {
            e.printStackTrace();            
        }
		
        System.out.println("\nMapping JSON fields...");
        JsonFactory factory = new JsonFactory();
		mapper = new ObjectMapper(factory);
		
		rootNode = null;
		try {
			rootNode = mapper.readTree(schema.toString());
			
			System.out.println(
					"*******************************************************************************\n" +
					"Root Node Dump:" +
					"\n*******************************************************************************\n"
					);
			System.out.println(rootNode.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
		return rootNode;
	}
	
	

	//*************************************************************************
	//*************************************************************************
	//*************************************************************************
	//*************************************************************************
	//*************************************************************************
	
	
	/**************************************************************************
	 * 
	 *************************************************************************/
	@Test
	public void RuntimeTaskInstance() 
	{
		String filename = ".\\\\ref\\\\RuntimeTaskInstance.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void StockTransferDocumentGenerated() 
	{
		String filename = ".\\\\ref\\\\StockTransferDocumentGenerated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**************************************************************************
	 * 
	 *************************************************************************/
	@Test
	public void StockTransferReceived() 
	{
		String filename = ".\\\\ref\\\\StockTransferReceived.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void StockTransferRequested() 
	{
		String filename = ".\\\\ref\\\\StockTransferRequested.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void StockTransferShipped() 
	{
		String filename = ".\\\\ref\\\\StockTransferShipped.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void AdvanceShipmentNoticeAdjusted() 
	{
		String filename = ".\\\\ref\\\\AdvanceShipmentNoticeAdjusted.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void AdvanceShipmentNoticePersisted() 
	{
		String filename = ".\\\\ref\\\\AdvanceShipmentNoticePersisted.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void CreateTaskResult() 
	{
		String filename = ".\\\\ref\\\\CreateTaskResult.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void HandlingUnitUpdated() 
	{
		String filename = ".\\\\ref\\\\HandlingUnitUpdated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void LapseGoodsReceipt() 
	{
		String filename = ".\\\\ref\\\\LapseGoodsReceipt.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void LapseLocation() 
	{
		String filename = ".\\\\ref\\\\LapseLocation.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void PurchaseOrderCSOSSigningStatusUpdated() 
	{
		String filename = ".\\\\ref\\\\PurchaseOrderCSOSSigningStatusUpdated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReceiveGoods() 
	{
		String filename = ".\\\\ref\\\\ReceiveGoods.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void CreateReturnsClaim() 
	{
		String filename = ".\\\\ref\\\\CreateReturnsClaim.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimAuthorizationApprovalReceived() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimAuthorizationApprovalReceived.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimAuthorizationRequestCreated() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimAuthorizationRequestCreated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimDocumentActivity() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimDocumentActivity.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimDocumentReceived() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimDocumentReceived.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimItemShipped() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimItemShipped.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimConfirmed() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimConfirmed.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimCreated() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimCreated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimItemAddOrUpdated() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimItemAddOrUpdated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimShipped() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimShipped.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void ReturnsClaimUpdated() 
	{
		String filename = ".\\\\ref\\\\ReturnsClaimUpdated.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	
	
	
	
	/**
	 * 
	 */
	@Test
	public void AdvanceShipmentNoticeAdjusted_test1() 
	{
		String filename = ".\\\\ref\\\\AdvanceShipmentNoticeAdjusted_test1.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	/**
	 * 
	 */
//	@Test
	public void AdvanceShipmentNoticeAdjusted_test2() 
	{
		String filename = ".\\\\ref\\\\AdvanceShipmentNoticeAdjusted_test2.avsc";
		JsonNode rootNode = initialize(filename);
		useJsonMapperForFieldsOnly(rootNode);
		useJsonMapperForSchemaObject(rootNode);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * @param rootNode
	 */
	public void useJsonMapperForFieldsOnly(JsonNode rootNode)
	{
		if(!fieldsOnly)
		{
			return;
		}
		System.out.println(
				"*******************************************************************************\n" +
				Thread.currentThread().getStackTrace()[1].getMethodName() +
				"\n*******************************************************************************"
				);
		
       Iterator<Map.Entry<String,JsonNode>> fieldsIterator = rootNode.fields();
//       while (fieldsIterator.hasNext()) 
//       {
//           Map.Entry<String,JsonNode> field = fieldsIterator.next();
//           System.out.println("Key: " + field.getKey() + "\tValue datatype:" + field.getValue().getClass() + "\tValue:" + field.getValue() );
//       }
//       System.out.println("\n\n");
//       fieldsIterator = null;
//       fieldsIterator = rootNode.fields();
       
       while (fieldsIterator.hasNext()) 
       {
           Map.Entry<String,JsonNode> field = fieldsIterator.next();
           if (field.getValue().isArray() && field.getKey() == "fields") 
           {
               System.out.println("field name: " + field.getKey() + "\nIs this node an Array? " + field.getValue().isArray());
               System.out.println("");
               for(JsonNode jn : field.getValue())
               {
            	   System.out.println(jn.toString());
            	   
            	   AvroSchemaFieldMapper asfm = mapper.convertValue(jn,AvroSchemaFieldMapper.class);
            	   
            	   System.out.println(asfm.toString());
            	   System.out.println(asfm.toCsvFormat() + "\n");
//            	   break;
               }
           }
       }
       
       
       
	}//end useJsonMapperForFieldsOnly
	
	
	/**
	 * @param rootNode
	 */
	public void useJsonMapperForSchemaObject(JsonNode rootNode)
	{
		if(schemaOnly)
		{
			System.out.println(
					"*******************************************************************************\n" +
					Thread.currentThread().getStackTrace()[1].getMethodName() +
					"\n*******************************************************************************"
					);
			
			AvroSchemaMapper asm = mapper.convertValue(rootNode,AvroSchemaMapper.class);
			System.out.println(asm.toString());
			System.out.println(asm.toCsvFormat());
		}
		
	}//end useJsonMapperForSchemaObject
	
	
	
	
	
	
	
	
	
	
//	@Test
	public void zzz_testWithGson(String uglyJSONString)
	{
		System.out.println(
				"*******************************************************************************\n" +
				Thread.currentThread().getStackTrace()[1].getMethodName() +
				"\n*******************************************************************************"
				);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(uglyJSONString);
		String prettyJsonString = gson.toJson(je);
		System.out.println(prettyJsonString);
	}
	
	
//	@Test
	public void zzz_testWithRecursion(final JsonNode node)
	{

		System.out.println(
				"*******************************************************************************\n" +
				Thread.currentThread().getStackTrace()[1].getMethodName() +
				"\n*******************************************************************************"
				);
		
		System.out.println("Node type: " + node.getNodeType() + " : " + node.getClass());
		
		
		Iterator<Map.Entry<String, JsonNode>> fieldsIterator = node.fields();

	    while (fieldsIterator.hasNext()) {
	        Map.Entry<String, JsonNode> field = fieldsIterator.next();
	        final String key = field.getKey();
	        System.out.println("Key: " + key);
	        final JsonNode value = field.getValue();
	        System.out.println("Value: " + value + " : " + value.getNodeType());
	        
	        if(value.getNodeType() == JsonNodeType.STRING)
	        	System.out.println("Value: " + value + " : " + value.getNodeType() +  " : " + value.toString());
	        else if (value.getNodeType() == JsonNodeType.ARRAY)
	        	zzz_testWithRecursion(value); // RECURSIVE CALL
	        else {
	            System.out.println("Value: " + value + " : " + value.getNodeType() +  " : " + value.toString());
	        }
	        
	        
	        
	        
	        
//	        if (value.isContainerNode() || value instanceof ArrayNode) {
//	        	print("inside arraynode");
//	        	zzz_testWithRecursion(value); // RECURSIVE CALL
//	        } 
//	        else if(value instanceof Array)
//	        {
//	        	mapper.convertValue(value, ArrayList.class);
//	        }
//	        else {
//	            System.out.println("Value: " + value + " : " + value.getNodeType() +  " : " + value.toString());
//	        }
	    }
	    
	    
	    
	    /*
	     * RootNode = ContainerNode
	     *     + Get 
	     * 
	     * 
	     * 
	     * 
	     * 
	     * 
	     */
	    
	}
	
	
	
	
	
	
	/*
	 * 
	 *   UTILITY METHODS 
	 */
	
	
	public static void print(String s)
	{
		System.out.print(s);
	}
	
	public static void printFileTitle(File s)
	{
		System.out.println(
				"\n\n\n-------------------------------------------------------------------------------\nTest File: " +
						s.getName() + 
				"\n-------------------------------------------------------------------------------"
				);
	}
	
	
	/*
	 * REFERNECE
	 */

	
//	@BeforeClass
//	@Test
	public void test0_all() 
	{
		String pathToFile = "";
		//TODO: Change to read all files in ref directory
		@SuppressWarnings("serial")
		ArrayList<String> testFiles = new ArrayList<String>()
			{{
				//Testing files
//				add(".\\ref\\RuntimeTaskInstance.avsc");
//				add(".\\ref\\StockTransferDocumentGenerated.avsc");
//				add(".\\ref\\StockTransferReceived.avsc");
//				add(".\\ref\\StockTransferRequested.avsc");
//				add(".\\ref\\StockTransferShipped.avsc");

//				add(".\\ref\\AdvanceShipmentNoticeAdjusted.avsc");
//				add(".\\ref\\AdvanceShipmentNoticePersisted.avsc");
//				add(".\\ref\\CreateTaskResult.avsc");
//				add(".\\ref\\HandlingUnitUpdated.avsc");
//				add(".\\ref\\LapseGoodsReceipt.avsc");
//				add(".\\ref\\LapseLocation.avsc");
//				add(".\\ref\\PurchaseOrderCSOSSigningStatusUpdated.avsc");
//				add(".\\ref\\ReceiveGoods.avsc");

//				add(".\\ref\\AdvanceShipmentNoticeAdjusted_test1.avsc");
				add(".\\ref\\AdvanceShipmentNoticeAdjusted_test2.avsc");
				
				//Requested files:
//				add(".\\ref\\CreateReturnsClaim.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimAuthorizationApprovalReceived.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimAuthorizationRequestCreated.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimDocumentActivity.avsc");
//				add(".\\ref\\ReturnsClaimDocumentReceived.avsc");
//				add(".\\ref\\ReturnsClaimItemShipped.avsc");
//				add(".\\ref\\ReturnsClaimConfirmed.avsc");  //totalCost
//				add(".\\ref\\ReturnsClaimCreated.avsc"); //multiple
//				add(".\\ref\\ReturnsClaimItemAddOrUpdated.avsc"); //multiple
//				add(".\\ref\\ReturnsClaimShipped.avsc");  //multiple
//				add(".\\ref\\ReturnsClaimUpdated.avsc"); //multiple
				
				
				//not working
				
				
				
			}}
		;
		//testing files
//		pathToFile = ".\\ref\\RuntimeTaskInstance.avsc"; //WORKS
//		pathToFile = ".\\ref\\StockTransferDocumentGenerated.avsc"; //works
//		pathToFile = ".\\ref\\StockTransferReceived.avsc"; //works
//		pathToFile = ".\\ref\\StockTransferRequested.avsc";
//		pathToFile = ".\\ref\\StockTransferShipped.avsc";
//		pathToFile = ".\\ref\\StockTransferUpdated.avsc";
//		pathToFile = ".\\ref\\TransferCreatedUpdated.avsc";
		
		
		for(String filename : testFiles)
		{
			JsonNode rootNode = initialize(filename);
			
			useJsonMapperForFieldsOnly(rootNode);
			
			useJsonMapperForSchemaObject(rootNode);
			
//			zzz_testWithGson(rootNode.toString());
//			zzz_testWithRecursion(rootNode);
			
			rootNode = null;
		}
	}// end test0_all
	
	
	
	
	
	
}//end class
