package fragments

import tornadofx.*
import javafx.beans.property.SimpleStringProperty

class PopupFragment(): Fragment() {
//    val message: String by param()
//    val popupTitle:String by param()
    
    val message = SimpleStringProperty()
//    super.title = thisTitle.toString()
    
    override val root = label(message)
    {
//        super.title = thisTitle.toString()
        prefHeight = 50.0
        prefWidth = 250.0
    }
}
