package com.wag.cosmosdb.workers;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.data.cosmos.ChangeFeedProcessor;
import com.azure.data.cosmos.ChangeFeedProcessorOptions;
import com.azure.data.cosmos.ConnectionMode;
import com.azure.data.cosmos.ConnectionPolicy;
import com.azure.data.cosmos.CosmosClient;
import com.azure.data.cosmos.CosmosContainer;
import com.azure.data.cosmos.CosmosDatabase;
import com.azure.data.cosmos.CosmosItemProperties;
import com.demo.ChangeFeedDemo;
import com.wag.cosmosdb.Constants;
import com.wag.cosmosdb.CosmosMethods;

import reactor.core.scheduler.Schedulers;

public class CheckChangeFeed {
	
	private final static Logger log = LoggerFactory.getLogger(CheckChangeFeed.class);

	
	
	public static void main(String[] args) 
	{
		log.info("Starting CheckChangeFeed");
		//Get current time for timing
		Instant processStartTime = Instant.now();
		
		ConnectionPolicy connectionPolicy = new ConnectionPolicy();
    	connectionPolicy.connectionMode(ConnectionMode.GATEWAY);
    	
        final CosmosClient client = CosmosClient.builder()
        		.endpoint(Constants.ONLINE_HOST)
        		.key(Constants.ONLINE_MASTER_KEY)
        		.connectionPolicy(connectionPolicy)
        		.build();

        //Get and create source database
        log.info("Confirm source database: " + Constants.sourceDatabaseName);
        CosmosMethods.createDatabase(client, Constants.sourceDatabaseName);
        CosmosDatabase sourceDatabase = client.getDatabase(Constants.sourceDatabaseName);
        CosmosMethods.createContainer(sourceDatabase, Constants.sourceContainerName);
        CosmosContainer sourceContainer = sourceDatabase.getContainer(Constants.sourceContainerName);
        

        //Get and create lease database
        log.info("Create lease database: " + Constants.leaseDatabaseName);
        CosmosMethods.createDatabase(client, Constants.leaseDatabaseName);
        CosmosDatabase leaseDatabase = client.getDatabase(Constants.leaseDatabaseName);
        CosmosMethods.createContainer(leaseDatabase, Constants.leaseContainerName, "/id");
        CosmosContainer leaseContainer = leaseDatabase.getContainer(Constants.leaseContainerName);
        
        
        //Try to get target databases
        log.info("Create target database: " + Constants.DATABASE_NAME_PrescriptionByCode);
        CosmosMethods.createDatabase(client, Constants.DATABASE_NAME_PrescriptionByCode);
        CosmosDatabase PrescriptionByCodeDatabase = client.getDatabase(Constants.DATABASE_NAME_PrescriptionByCode);
        CosmosMethods.createContainer(PrescriptionByCodeDatabase, Constants.CONTAINER_NAME_PrescriptionByCode,"/prescriptionCode");
        CosmosContainer PrescriptionByCodeContainer = PrescriptionByCodeDatabase.getContainer(Constants.CONTAINER_NAME_PrescriptionByCode);
        

        log.info("Create target database: " + Constants.DATABASE_NAME_PrescriptionByExternalDataEnteredBy);
        CosmosMethods.createDatabase(client, Constants.DATABASE_NAME_PrescriptionByExternalDataEnteredBy);
        CosmosDatabase PrescriptionByExternalDataEnteredByDatabase = 
        		client.getDatabase(Constants.DATABASE_NAME_PrescriptionByExternalDataEnteredBy);
        CosmosMethods.createContainer(PrescriptionByExternalDataEnteredByDatabase, 
        		Constants.CONTAINER_NAME_PrescriptionByExternalDataEnteredBy,
        		"/externalDataEnteredBy/lastName");
        CosmosContainer PrescriptionByExternalDataEnteredByContainer = 
        		PrescriptionByExternalDataEnteredByDatabase.getContainer(Constants.CONTAINER_NAME_PrescriptionByExternalDataEnteredBy);
        

        log.info("Create target database: " + Constants.DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        CosmosMethods.createDatabase(client, Constants.DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        CosmosDatabase PrescriptionByExternalLastDispenseEnteredDateTimeDatabase = 
        		client.getDatabase(Constants.DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        CosmosMethods.createContainer(PrescriptionByExternalLastDispenseEnteredDateTimeDatabase, 
        		Constants.CONTAINER_NAME_PrescriptionByExternalLastDispenseEnteredDateTime,
        		"/externalLastDispenseEnteredDateTime");
        CosmosContainer PrescriptionByExternalLastDispenseEnteredDateTimeContainer = 
        		PrescriptionByExternalLastDispenseEnteredDateTimeDatabase.getContainer(Constants.CONTAINER_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        
        
		// READ change feed from current.
//        ChangeFeedOptions changeFeedOption = new ChangeFeedOptions();
//        changeFeedOption.maxItemCount(10);
        
        //*********************************************************************
        log.info("Building ChangeFeedProcessor...");
        ChangeFeedProcessor changeFeedProcessor = ChangeFeedProcessor.Builder()
                .hostName(Constants.ONLINE_HOST)
                .feedContainer(sourceContainer)
                .leaseContainer(leaseContainer) 
                .options(new ChangeFeedProcessorOptions()
                    .leaseRenewInterval(Duration.ofSeconds(20))
                    .leaseAcquireInterval(Duration.ofSeconds(10))
                    .leaseExpirationInterval(Duration.ofSeconds(3600))
                    .feedPollDelay(Duration.ofSeconds(5))
//                    .leasePrefix("Running against: ")
                    .maxItemCount(10000)
                    .startFromBeginning(false)
                    
                    .maxScaleCount(0) // unlimited
//                    .discardExistingLeases(true)
                )
                .handleChanges(docs -> {
                    log.info("START processing from thread {}", Thread.currentThread().getId());
//                    removeDatabaseRecordFromList(docs);
                    log.info(docs.toArray().toString());
                    if(docs.size() > 0)
                    {
                        
	                    for (CosmosItemProperties item : docs) {
	                    	log.info("I have item: " + item.id());

	                    	CosmosMethods.createItem(PrescriptionByCodeContainer, item);
	                    	CosmosMethods.createItem(PrescriptionByExternalDataEnteredByContainer, item);
	                    	CosmosMethods.createItem(PrescriptionByExternalLastDispenseEnteredDateTimeContainer, item);
	                        
	                        log.info("Item: " + item.id() + " created.");
	                    }

	                    Instant processEndTime = Instant.now();
                    	Duration lapsedProcessingTime = Duration.between(processStartTime, processEndTime);
                        String lapsedProcessingTimeString = formatDuration(lapsedProcessingTime);
                        System.out.println(
                    		"\n******************************************************************************\n" + 
                    		" ChangeFeed processing duration: " + lapsedProcessingTimeString + " for " + docs.size() + " records." +
                    		"\n******************************************************************************\n"
                        );
                    }
                    
                    log.info("END processing from thread {}", Thread.currentThread().getId());
                })
                .build()
        ;
        //*********************************************************************
        
        log.info("COMPLETE: ChangeFeedProcessor built...");
        log.info("Starting ChangeFeedProcessor");
        try {
            changeFeedProcessor.start().subscribeOn(Schedulers.elastic())
                .timeout(Duration.ofMillis(5000))
                .subscribe();
        } catch (Exception ex) {
            log.error("Change feed processor did not start in the expected time", ex);
        }
        
        
        if(!ChangeFeedDemo.runDemo)
        {
        	log.error("Press any key to close ChangeFeedProcessor ...");
			try {
				System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			heavyWork(10);
	        log.info("Stopping ChangeFeedProcessor");
	        changeFeedProcessor.stop().subscribeOn(Schedulers.elastic()).timeout(Duration.ofMillis(5000)).subscribe();
	        heavyWork(1);
	        log.info("Exiting CheckChangeFeed at: " + new Date());
	        Instant processEndTime = Instant.now();
	        

	        Duration lapsedProcessingTime = Duration.between(processStartTime, processEndTime);
	        String lapsedProcessingTimeString = formatDuration(lapsedProcessingTime);
	        

	        System.out.println(
	    		"\n******************************************************************************\n" + 
	    		" ChangeFeed processing duration: " + lapsedProcessingTimeString + 
	    		"\n******************************************************************************\n"
	        );
	        
        	System.exit(0);
        }
        else
        {

			heavyWork(30);
			log.info("Stopping ChangeFeedProcessor");
			changeFeedProcessor.stop().subscribeOn(Schedulers.elastic()).timeout(Duration.ofMillis(5000)).subscribe();
			heavyWork(1);
			log.info("Exiting CheckChangeFeed at: " + new Date());
			Instant processEndTime = Instant.now();
			Duration lapsedProcessingTime = Duration.between(processStartTime, processEndTime);
			String lapsedProcessingTimeString = formatDuration(lapsedProcessingTime);
			
			
			System.out.println(
		    		"\n******************************************************************************\n" + 
		    		" ChangeFeed processing duration: " + lapsedProcessingTimeString + 
		    		"\n******************************************************************************\n"
		        );
        }
//        changeFeedProcessor.stop();
        return;
        
	}
	
	public static String formatDuration(Duration duration) {
	    long seconds = duration.getSeconds();
	    long absSeconds = Math.abs(seconds);
	    String positive = String.format(
	        "%d:%02d:%02d",
	        absSeconds / 3600,
	        (absSeconds % 3600) / 60,
	        absSeconds % 60);
	    return seconds < 0 ? "-" + positive : positive;
	}
	
	
	/**
	 * @param seconds
	 */
	public static void heavyWork(int seconds) {
        // I may do a lot of IO work: e.g., writing to log files
        // a lot of computational work
        // or may do Thread.sleep()

        try {
        	log.info("\n\nSleeping for " + seconds + " seconds...\n" );
            TimeUnit.SECONDS.sleep(seconds);
        } catch (Exception e) {
        }
    }
	
	
	
	/**
	 * This method is required due to a database description record being included
	 * in the list that returns for the handleChanges method.
	 * @param docs
	 */
	private static synchronized void removeDatabaseRecordFromList(List<CosmosItemProperties> docs) 
	{
		log.info("Entering removeDatabaseRecordFromList...");
	    for(CosmosItemProperties item : docs)
	    {
	    	if( ((String) item.get("id")).contains(Constants.sourceDatabaseName)  )
	    	{
	    		docs.remove(item);
	    	}
	    }
	}
	
    
}
