@echo off

echo.
echo Pushing to remote Bitbucket repo...
echo.
::Move up a level to root directory to capture ALL changes in the repo
cd ..
call git push
echo Git push complete.
echo.
Pause
exit /b