package com.avro;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.avro.Schema;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.avro.mappers.*;


/******************************************************************************
 * @author falco
 *
 *****************************************************************************/
public class AvroExtractor {
	
	public static boolean debug = true;
//	public static boolean userInput = false;

//	public static boolean debug = false;
	public static boolean userInput = true;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
	
		String pathToFile = "";
		if(userInput)
		{
			System.out.println("What is the path to the AVRO (avsc) model file you want to generate from?:");
	    	Scanner in = new Scanner(System.in);
	        pathToFile = in.nextLine();
	        in.close();
	        
	        //Identify if the input is an avro file or a directory
	        ArrayList<String> testFiles = new ArrayList<String>();
	        File avroFile = new File(pathToFile);
	        //If avro file
	        if(avroFile.isFile())
	        {
	        	//  add to file list directly
	        	testFiles.add(pathToFile);
	        }
	        //else if directory
	        else
	        {
		        //  loop through each file in passed directory
	        	for(File dirFile : avroFile.listFiles())
	        	{
			        //    if file has avro extension
	        		if(dirFile.getName().endsWith(".avsc"))
	        		{
	        			//      add to file list
	        			testFiles.add(dirFile.getAbsolutePath());
	        		}
	        	}
	        }
	        

	        //loop through file list, execute convert method
	        //
        	for(String x : testFiles)
				convertTofile(x);
	        
	        
		}
		else
		{
			//Prodution testing area
			ArrayList<String> testFiles = new ArrayList<String>()
			{{

				//Original Testing files
//				add(".\\ref\\RuntimeTaskInstance.avsc");
//				add(".\\ref\\StockTransferDocumentGenerated.avsc");
//				add(".\\ref\\StockTransferReceived.avsc");
//				add(".\\ref\\StockTransferRequested.avsc"); //failed
//				add(".\\ref\\StockTransferShipped.avsc");
				
				//Requested files:
//				add(".\\ref\\CreateReturnsClaim.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimAuthorizationApprovalReceived.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimAuthorizationRequestCreated.avsc"); //PASS
//				add(".\\ref\\ReturnsClaimDocumentActivity.avsc");
//				add(".\\ref\\ReturnsClaimDocumentReceived.avsc");
//				add(".\\ref\\ReturnsClaimItemShipped.avsc");
//				add(".\\ref\\ReturnsClaimConfirmed.avsc");
//				add(".\\ref\\ReturnsClaimCreated.avsc");
//				add(".\\ref\\ReturnsClaimItemAddOrUpdated.avsc");
//				add(".\\ref\\ReturnsClaimShipped.avsc");
//				add(".\\ref\\ReturnsClaimUpdated.avsc");
				

				add(".\\ref\\AdvanceShipmentNoticeAdjusted.avsc");
//				add(".\\ref\\AdvanceShipmentNoticePersisted.avsc");
//				add(".\\ref\\CreateTaskResult.avsc");
//				add(".\\ref\\HandlingUnitUpdated.avsc");
//				add(".\\ref\\LapseGoodsReceipt.avsc");
//				add(".\\ref\\LapseLocation.avsc");
//				add(".\\ref\\PurchaseOrderCSOSSigningStatusUpdated.avsc");
//				add(".\\ref\\ReceiveGoods.avsc");
				
				
				//not working
				
				
			}};
			
			for(String x : testFiles)
				convertTofile(x);
			
			return;
		}
		
		
		
		
		
		
		
		
		
		//Normal processing
		
		
		
//        File avroFile = new File(pathToFile);
//        
//		System.out.println(avroFile.getName().substring(0,avroFile.getName().length()-5));
//		File outputFile = new File( (avroFile.getName().substring(0,avroFile.getName().length()-5)+".csv") );
//		
//		Schema.Parser parser = new Schema.Parser();
//        Schema schema = null;
//        try {
//            schema = parser.parse(avroFile);
//        } catch (IOException e) {
//            e.printStackTrace();            
//        }
//        
//		debug("\nMapping JSON fields...");
//        JsonFactory factory = new JsonFactory();
//		ObjectMapper mapper = new ObjectMapper(factory);
//		
//		JsonNode rootNode = null;
//		try {
//			rootNode = mapper.readTree(schema.toString());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}  
//		
//		AvroSchemaMapper asm = mapper.convertValue(rootNode,AvroSchemaMapper.class);
//		debug(asm.toString());
//        
//        //write to file
//		//output to file
//		
//		try{    
//			outputFile.delete();
//			
//			FileWriter fw=new FileWriter(outputFile);   
//        	fw.write(asm.toCsvFormat());
//        	fw.write("\n\n");
//			fw.close();
//		}
//		catch(Exception e){System.out.println(e);}    
		System.out.println("Processing Successful..."); 

		System.out.println("\n\n-----END-----");
    }
    
	
	
    /**
     * @param x
     */
    public static void debug(String x)
    {
    	if(debug)
    	{
    		System.out.println("DEBUG : " + x);
    	}
    }
    
    public static void typeIs(Object x)
    {
    	if(debug)
    	{
    		System.out.println("\tTYPE IS : " + x.getClass().getSimpleName());
    	}
    }
    
    
    public static void entering(String x)
    {
    	if(debug)
    	{
    		System.out.println("ENTER : " + x + "()");
    	}
    }
    
    public static void exiting(String x)
    {
    	if(debug)
    	{
    		System.out.println("EXIT  : " + x + "()");
    	}
    }
    
    
    
    
    
    /**
     * @param pathToFile
     */
    public static void convertTofile(String pathToFile)
    {

        File avroFile = new File(pathToFile);
        
		System.out.println(avroFile.getName().substring(0,avroFile.getName().length()-5));
		
		File outputDir = new File( ".\\output\\");
		File outputFile = new File( outputDir.getAbsolutePath()+"\\"+(avroFile.getName().substring(0,avroFile.getName().length()-5)+".csv") );
		
		Schema.Parser parser = new Schema.Parser();
        Schema schema = null;
        try {
            schema = parser.parse(avroFile);
        } catch (IOException e) {
            e.printStackTrace();            
        }
        
		debug("\nMapping JSON fields...");
        JsonFactory factory = new JsonFactory();
		ObjectMapper mapper = new ObjectMapper(factory);
		
		JsonNode rootNode = null;
		try {
			rootNode = mapper.readTree(schema.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		AvroSchemaMapper asm = mapper.convertValue(rootNode,AvroSchemaMapper.class);
		debug(asm.toString());
        
        //write to file
		//output to file
		
		try{    
			if(outputDir.exists());
			else
				outputDir.mkdir();
			
			outputFile.delete();
			
			FileWriter fw=new FileWriter(outputFile);   
        	fw.write(asm.toCsvFormat());
        	fw.write("\n\n");
			fw.close();
		}
		catch(Exception e){System.out.println(e);}    
		System.out.println("Success..."); 

		System.out.println("\n\n-----END-----");
    }
}
