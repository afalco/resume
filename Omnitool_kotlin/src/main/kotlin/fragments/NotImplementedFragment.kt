package fragments

import tornadofx.*

class NotImplementedFragment(): Fragment("Function Not Implemented") {
    override val root = label("This function is not yet developed.")
    {
        prefHeight = 50.0
        prefWidth = 250.0
    }
}