package com.microsoft.azure.cosmosdb.sample;

public enum EmployeeType
{
	CONSULTANT,
    CSR,
    EMPLOYEE,
    WAGBPO,
    WHSCSR
    ;
}