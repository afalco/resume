package com.wag.cosmosdb.workers;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.data.cosmos.ConnectionMode;
import com.azure.data.cosmos.ConnectionPolicy;
import com.azure.data.cosmos.CosmosClient;
import com.azure.data.cosmos.CosmosContainer;
import com.azure.data.cosmos.CosmosDatabase;
import com.github.javafaker.Faker;
import com.microsoft.azure.cosmosdb.sample.Employee;
import com.microsoft.azure.cosmosdb.sample.EmployeeType;
import com.microsoft.azure.cosmosdb.sample.Name;
import com.microsoft.azure.cosmosdb.sample.Prescription;
import com.wag.cosmosdb.Constants;
import com.wag.cosmosdb.CosmosMethods;

public class CreateRecords {
	
	private final static Logger log = LoggerFactory.getLogger(CreateRecords.class);

    /**
     * Run a Hello DocumentDB console application.
     *
     * @param args command line args.
     */
    public static void main(String[] args) {
    	
    	int numOfRecordsToCreate = 50;
    	
    	if(args != null && args.length !=0)
    	{
    		for(int i = 0; i < args.length; i++) {
                System.out.println(args[i]);
            }
    		
    		String argsString = args.toString();
    		if(argsString.contains("numOfRecordsToCreate"))
    		{
    			numOfRecordsToCreate = Integer.parseInt(argsString);
    		}
    	}

    	log.info("Using Azure Cosmos DB endpoint: \n" + Constants.ONLINE_HOST + "\nKey: " + Constants.ONLINE_MASTER_KEY);
        
    	ConnectionPolicy connectionPolicy = new ConnectionPolicy();
    	connectionPolicy.connectionMode(ConnectionMode.GATEWAY);
    	
        CosmosClient client = CosmosClient.builder()
        		.endpoint(Constants.ONLINE_HOST)
        		.key(Constants.ONLINE_MASTER_KEY)
        		.connectionPolicy(connectionPolicy)
        		.build();
        
        log.info("createDatabaseIfNotExists..." + connectionPolicy.defaultPolicy().toString());
        CosmosMethods.createDatabase(client,Constants.sourceDatabaseName);
        
        log.info("Number of databases: " + client.readAllDatabases().collectList().block().size());
        
        log.info("Get database: " + Constants.sourceDatabaseName);
        CosmosDatabase database = client.getDatabase(Constants.sourceDatabaseName);

        // Create the container if it does not exist yet
        CosmosMethods.createContainer(database, Constants.sourceContainerName);
        CosmosContainer container = database.getContainer(Constants.sourceContainerName);
        
        log.info("Creating " + numOfRecordsToCreate + " records...");
        for(int i = 1; i <=numOfRecordsToCreate; i++)
        {
        	Prescription p = createRandomPrescription() ;
        	
        	log.info("createItem: " + p.getId());
    	    CosmosMethods.createItem(container, p);
//    	    heavyWork();
        }
	    
        //Close client
        log.info("Closing client...");
        client.close();
    }
    

    //*************************************************************************************************
    
    
    /**
     * @return
     */
    public static Prescription createRandomPrescription() 
    {
    	UUID key = UUID.randomUUID();
    	Random r = new Random();
    	Faker faker = new Faker();
    	
    	Prescription newItem = new Prescription(
    			key.toString(),
    			(r.ints(10000,10500+1).limit(1).findFirst().getAsInt() + ""),//prescriptionCode, 
    			false,//autoRefill, 
    			new Name(faker.name().firstName(),faker.name().lastName()),//externalDataEnteredBy,
    			new Date(),//externalLastDispenseEnteredDateTime, 
    			r.ints(1,90+1).limit(1).findFirst().getAsInt(),//externalLastQuantityDispensed, 
    			new Employee(
    					r.ints(1,90+1).limit(1).findFirst().getAsInt(),
    					getRandomEmployeeType()
    					)//rxClosedBy
    	);
    	
    	return newItem;
    }
    
    /**
     * @return
     */
    private static EmployeeType getRandomEmployeeType()
    {
    	int whichType = new Random().ints(1,6).limit(1).findFirst().getAsInt();
    	
    	EmployeeType type = null;
    	
    	switch(whichType)
    	{
    	case 1:
    		
    		type = EmployeeType.CONSULTANT;
    	case 2:
    		type = EmployeeType.CSR;
    	case 3:
    		type = EmployeeType.EMPLOYEE;
    	case 4:
    		type = EmployeeType.WAGBPO;
    	case 5:
    		type = EmployeeType.WHSCSR;
    	default:
    		break;
    	}
    	
    	return type;
    }
    
    
    
}