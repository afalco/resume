package controllers.tabs

import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller
import views.StatusBar
import java.io.File

class FileCreatorTabController: Controller(){
    val statusBar: StatusBar by inject()
    
//    fun createFiles(directory: String, numOfFiles: String)
    fun createFiles(directory: SimpleStringProperty, numOfFiles: SimpleStringProperty, fileType: String)
    {
        if(directory.value == null || numOfFiles.value == null) {
        	var message = "Missing values: "
            if(directory.value == null) message += "directory; "
            if(numOfFiles.value == null) message += "number of files; "
            statusBar.updateStatusBarMessage_error(message)
            return
        }
        
        var numOfFiles = numOfFiles.value  //number
        var directory = directory.value //string
        
        //Check for proper number formatting
        try {
            println("Trying to parse integer of: " + numOfFiles)
            Integer.parseInt(numOfFiles);
        }
        catch(e : Exception) {
            statusBar.updateStatusBarMessage_error("Number of files is not a number.");
//            throw NumberFormatException("Number of files is not a number.")
            return
        }
        
        //Check for directory formatting
        if(directory.contains(":\\")) {
            
//            println(directory)
//            println(Regex("[A-Z]\\:\\[\\]+"))
            println(directory.matches(Regex("[A-Z]\\:\\\\[\\\\]+")))
            
            if(directory.matches(Regex("[A-Z]\\:\\\\+")) ) {
                println(directory)
                statusBar.updateStatusBarMessage_error("Directory is a root directory, cannot create or create files in root directory.");
                return
            }
            
            //Check for directory existence
            var dir = File(directory)
            if (!dir.exists()) {
                statusBar.updateStatusBarMessage("Creating directory: " + directory);
                dir.mkdirs()
            }
        }
        else {
            statusBar.updateStatusBarMessage_error("Directory is not properly formatted.");
//            throw NumberFormatException("Number of files is not a number.")
            return
        }
        
        //Create files now
        statusBar.updateStatusBarMessage("Creating $numOfFiles files in $directory")
        var numberOfFilesToMake = Integer.parseInt(numOfFiles)
        
        for(i in 1..numberOfFilesToMake)
        {
            var file = File(directory + "\\" + i + "." + fileType)
            if (!file.exists()) {
                println("Creating file: " + file.absolutePath)
                try {
                	file.createNewFile()
                }
                catch(e : Exception) {
                    statusBar.updateStatusBarMessage_error("Exception found: " + e.getLocalizedMessage());
                    throw e
                }
            }
        }
        
        
        
        
        
    }//end createFiles
    
    
    
    
    
    
    
    
    
} //end FileCreatorTabController
