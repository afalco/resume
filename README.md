# Anthony Falco - Resume

This repo contains code examples of non-proprietary code for use with my resume.

My standard resume can be found with an open Google Docs link [here](https://drive.google.com/open?id=1qcjy6Rochdzh9l8yKUzFvAL_gwT6wmTyKBB2KClT1BY).