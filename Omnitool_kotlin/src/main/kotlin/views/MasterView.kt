package views

//import controllers.MasterController
import tornadofx.*


class MasterView : View()
{
    val VERSION = "2.0.0";
    val APP_TITLE = "Omnitool v"+VERSION+" using TornadoFx";
    
//    val controller: MasterController by inject()
    
    // Explicitly retrieve TopView
    val menuBarView = find(MasterMenuBar::class)
    // Create a lazy reference to BottomView
    val statusBar: StatusBar by inject()
    // Create a lazy reference to CenterView
    val centerView: CenterView by inject()
    
    //Begin root pane
    override val root = borderpane {
        
        title = APP_TITLE
        
        prefHeight = 500.0
        prefWidth = 750.0
        
        //Add the overall menu bar
    	top = menuBarView.root
        //Add the bottom Status bar
    	bottom = statusBar.root
        
        
        center = centerView.root
        
        
        
        
    }//end root pane
}

