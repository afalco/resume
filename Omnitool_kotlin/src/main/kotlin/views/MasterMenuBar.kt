package views

import controllers.MasterController
//import models.MasterModel
//import styles.EnabledMenuOptionStyle
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.checkbox
import tornadofx.hbox
import tornadofx.hide
import tornadofx.item
import tornadofx.menu
import tornadofx.menubar
import tornadofx.removeClass
import tornadofx.separator
import tornadofx.show

class MasterMenuBar: View() {
    val mc: MasterController by inject()
//    val cv: CenterView by inject()
    
    override val root = //hbox {
        menubar{
			menu("File") {
				menu("Connect") {
					item("Facebook")
					item("Twitter")
				}
	            
				separator()
	            
				item("Save").action {
	                mc.notImplementedPopup()
	            }
				item("Quit").action {
	                mc.notImplementedPopup()
	            }
			}
			menu("Edit") {
				item("Copy").action {
	                mc.notImplementedPopup()
	            }
				item("Paste").action {
	                mc.notImplementedPopup()
	            }
			}
	        
	        menu("Help") {
	            item("About").action {
	                mc.notImplementedPopup()
	            }
	        }
            
	    }//end menubar
        
        
        
        
        
    //}//end hbox
    
}//end class