package com.demo;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.wag.cosmosdb.workers.CheckChangeFeed;
import com.wag.cosmosdb.workers.CreateRecords;

/**
 *
 * Our goal is to create a small working PoC that can demonstrate how 
 * ChangeFeed works preferably using java SDK.
 * 
 *
 *
 * Database organization
 * 1. Source: PrescriptionRaw DB
 *   a. Collection: PrescriptionCollection
 * 2. Target 1: PrescriptionByCode
 *   a. Collection: ByCode
 * 3. Target 2: PrescriptionByExternalDataEnteredBy
 *   a. Collection: ByName
 * 4. Target 3: PrescriptionByExternalLastDispenseEnteredDateTime
 *   a. Collection: ByDate
 *   
 *  
 *   
 */
public class ChangeFeedDemo {

	public static boolean runDemo = true;
	public static boolean runPopulateRecords = true;
	public static boolean runChangeFeedRunner = true;
	
	public static void main(String[] args) 
	{
		int secondsBetweenFunctions = 10;
		
		if(args.length != 0)
		{
			secondsBetweenFunctions = Integer.parseInt(args[0]);
		}
		
		PopulateRecords pr = new PopulateRecords(1);
		if(runPopulateRecords)
		{
			pr.start();
		}
		heavyWork(secondsBetweenFunctions);
		ChangeFeedRunner cfr = new ChangeFeedRunner(10);
		if(runChangeFeedRunner)
		{
			cfr.start();
		}
		
		try {
			System.out.println("Press any key to close demo ...");
			System.in.read();
			if(runPopulateRecords)
			{
				pr.stop();
			}
			if(runChangeFeedRunner)
			{
				cfr.stop();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Main thread exiting.");
		System.exit(0);
	}
	
	/**
	 * @param seconds
	 */
	public static void heavyWork(int seconds) {
        // I may do a lot of IO work: e.g., writing to log files
        // a lot of computational work
        // or may do Thread.sleep()

        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (Exception e) {
        }
    }
}


class PopulateRecords implements Runnable
{
	private Thread worker;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private int interval;

	public PopulateRecords(int sleepInterval) {
        interval = sleepInterval*1000;
    }
	
	public void start() {
        worker = new Thread(this);
        worker.start();
    }
  
    public void stop() {
        running.set(false);
    }
    
    public void interrupt() {
        running.set(false);
        worker.interrupt();
    }
 
    boolean isRunning() {
        return running.get();
    }
    
	@Override
	public void run()
	{
		int i = 0;
		running.set(true);
		while(running.get())
		{
			try {
				System.out.println("\n\n\n\nPopulateRecords running " + i);
				i++;
				
				CreateRecords.main(null);
				
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\nPopulateRecords main thread exiting... " + i);
	}
}


class ChangeFeedRunner implements Runnable
{
	private Thread worker;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private int interval;

	public ChangeFeedRunner(int sleepInterval) {
        interval = sleepInterval*1000;
    }
	
	public void start() {
        worker = new Thread(this);
        worker.start();
    }
  
    public void stop() {
        running.set(false);
    }
    
    public void interrupt() {
        running.set(false);
        worker.interrupt();
    }
 
    boolean isRunning() {
        return running.get();
    }
    
	@Override
	public void run()
	{
		int i = 0;
		running.set(true);
		while(running.get())
		{
			try {
				System.out.println("\n\n\nCheckChangeFeed running " + i);
				i++;
				
				CheckChangeFeed.main(null);
				
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\nCheckChangeFeed main thread exiting... " + i);
	}
}