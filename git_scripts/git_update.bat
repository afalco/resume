@echo off

For /F %%A In ('powershell Get-Date -format s') Do Set "MYUPDATETIME=%%A"

echo.
echo Committing to local git repo...
echo.
SET /p DESC=Enter specific description for commit: 
echo.
::Move up a level to root directory to capture ALL changes in the repo
cd ..
call git add *
call git commit -m "%MYUPDATETIME% - %USERNAME% - %DESC%"
echo Git commit complete.
echo.
Pause
exit /b