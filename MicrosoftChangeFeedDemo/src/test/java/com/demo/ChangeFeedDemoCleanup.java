package com.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.data.cosmos.ConnectionMode;
import com.azure.data.cosmos.ConnectionPolicy;
import com.azure.data.cosmos.CosmosClient;
import com.wag.cosmosdb.Constants;
import com.wag.cosmosdb.CosmosMethods;

public class ChangeFeedDemoCleanup {

	private final static Logger log = LoggerFactory.getLogger(ChangeFeedDemoCleanup.class);
	

	/**
	 * Test used to clean up WBA cloud database
	 */
	@Test
	public void onlineDatabaseCleanup() 
	{

    	ConnectionPolicy connectionPolicy = new ConnectionPolicy();
    	connectionPolicy.connectionMode(ConnectionMode.GATEWAY);
    	
        CosmosClient client = CosmosClient.builder()
        		.endpoint(Constants.ONLINE_HOST)
        		.key(Constants.ONLINE_MASTER_KEY)
        		.connectionPolicy(connectionPolicy)
        		.build();
        
        List<String> databasesToDrop = new ArrayList<String>(){{
        	add(Constants.sourceDatabaseName);
        	add(Constants.leaseDatabaseName);

        	add(Constants.DATABASE_NAME_PrescriptionByCode);
        	add(Constants.DATABASE_NAME_PrescriptionByExternalDataEnteredBy);
        	add(Constants.DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        }};

        
        System.out.println(""); //this for output's sake
        for(String databaseName : databasesToDrop)
        {
	        try
	        {
	        	log.info(" Dropping database: " + databaseName);
	        	CosmosMethods.dropDatabase(client.getDatabase(databaseName));
	        	log.info(" Database dropped: " + databaseName);
	        }
	        catch(Exception e)
	        {
	        	if(e.getMessage().contains("NotFound"))
	        	{
	        		log.warn(databaseName + " not found in CosmosDB at: " + Constants.ONLINE_HOST);
	        	}
	        	else
	        		e.printStackTrace();
	        }
        }
		
		
		System.out.println("\n Main thread exiting.");
//		System.exit(0);
	}
	
	
	
	
	/**
	 * Test used to clean up local emulator database
	 */
//	@Test
	public void localDatabaseCleanup() 
	{

    	ConnectionPolicy connectionPolicy = new ConnectionPolicy();
//    	connectionPolicy.connectionMode(ConnectionMode.);
    	connectionPolicy.enableEndpointDiscovery(false);
    	
    	
        CosmosClient client = CosmosClient.builder()
        		.endpoint(Constants.LOCAL_HOST)
        		.key(Constants.LOCAL_MASTER_KEY)
//        		.connectionPolicy(connectionPolicy)
//        		.consistencyLevel(ConsistencyLevel.SESSION)
        		.build();
        
        List<String> databasesToDrop = new ArrayList<String>(){{
        	add(Constants.sourceDatabaseName);
//        	add(Constants.leaseDatabaseName);

//        	add(Constants.DATABASE_NAME_PrescriptionByCode);
//        	add(Constants.DATABASE_NAME_PrescriptionByExternalDataEnteredBy);
//        	add(Constants.DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime);
        }};

        
        System.out.println(""); //this for output's sake
        for(String databaseName : databasesToDrop)
        {
	        try
	        {
	        	log.info(" Dropping database: " + databaseName);
	        	CosmosMethods.dropDatabase(client.getDatabase(databaseName));
	        	log.info(" Database dropped: " + databaseName);
	        }
	        catch(Exception e)
	        {
	        	if(e.getMessage().contains("NotFound"))
	        	{
	        		log.warn(databaseName + " not found in CosmosDB at: " + Constants.LOCAL_HOST);
	        	}
	        	else
	        		e.printStackTrace();
	        }
        }
		
		
		System.out.println("\n Main thread exiting.");
//		System.exit(0);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//*************************************************************************
	public static void heavyWork(int seconds) {
        // I may do a lot of IO work: e.g., writing to log files
        // a lot of computational work
        // or may do Thread.sleep()

        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (Exception e) {
        }
    }
}
