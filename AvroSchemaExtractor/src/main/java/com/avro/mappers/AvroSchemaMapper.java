package com.avro.mappers;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AvroSchemaMapper
{
	public String type;
	public String name;
	public String namespace;
	public String doc;
	public String version;
//	@JsonIgnore
	public ArrayList<AvroSchemaFieldMapper> fields = new ArrayList<AvroSchemaFieldMapper>();

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AvroSchemaMapper [type=" + type + ", name=" + name + ", namespace=" + namespace + ", doc=" + doc
				+ ", version=" + version + ", fields=" + fields + "]";
	}



	/**
	 * @return
	 */
	public String toCsvFormat()
	{
		String recordString = "";
		String returnString = "Table Name,Namespace,Field Name,Field Type,Description,Default Value\n";
		for(AvroSchemaFieldMapper x : fields)
		{
			recordString += name + "," + namespace + "," + x.toCsvFormat() + "\n";
			if(x.getType().startsWith("record")) System.out.println(x.getType()); 
			
			returnString += recordString;
			
			recordString = "";
		}
		
		return returnString;
	}
	
	
}
