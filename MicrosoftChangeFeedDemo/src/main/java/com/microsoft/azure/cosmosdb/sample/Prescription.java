package com.microsoft.azure.cosmosdb.sample;

import java.util.Date;

public class Prescription 
{
	String id = "";
	String prescriptionCode = "";
	boolean autoRefill = false;
	Name externalDataEnteredBy;
	Date externalLastDispenseEnteredDateTime;
	int externalLastQuantityDispensed = 0;
	Employee rxClosedBy;
	public Prescription(String id, String prescriptionCode, boolean autoRefill, Name externalDataEnteredBy,
			Date externalLastDispenseEnteredDateTime, int externalLastQuantityDispensed, Employee rxClosedBy) {
		super();
		this.id = id;
		this.prescriptionCode = prescriptionCode;
		this.autoRefill = autoRefill;
		this.externalDataEnteredBy = externalDataEnteredBy;
		this.externalLastDispenseEnteredDateTime = externalLastDispenseEnteredDateTime;
		this.externalLastQuantityDispensed = externalLastQuantityDispensed;
		this.rxClosedBy = rxClosedBy;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	public boolean isAutoRefill() {
		return autoRefill;
	}
	public void setAutoRefill(boolean autoRefill) {
		this.autoRefill = autoRefill;
	}
	public Name getExternalDataEnteredBy() {
		return externalDataEnteredBy;
	}
	public void setExternalDataEnteredBy(Name externalDataEnteredBy) {
		this.externalDataEnteredBy = externalDataEnteredBy;
	}
	public Date getExternalLastDispenseEnteredDateTime() {
		return externalLastDispenseEnteredDateTime;
	}
	public void setExternalLastDispenseEnteredDateTime(Date externalLastDispenseEnteredDateTime) {
		this.externalLastDispenseEnteredDateTime = externalLastDispenseEnteredDateTime;
	}
	public int getExternalLastQuantityDispensed() {
		return externalLastQuantityDispensed;
	}
	public void setExternalLastQuantityDispensed(int externalLastQuantityDispensed) {
		this.externalLastQuantityDispensed = externalLastQuantityDispensed;
	}
	public Employee getRxClosedBy() {
		return rxClosedBy;
	}
	public void setRxClosedBy(Employee rxClosedBy) {
		this.rxClosedBy = rxClosedBy;
	}
	@Override
	public String toString() {
		return "Prescription [id=" + id + ", prescriptionCode=" + prescriptionCode + ", autoRefill=" + autoRefill
				+ ", externalDataEnteredBy=" + externalDataEnteredBy + ", externalLastDispenseEnteredDateTime="
				+ externalLastDispenseEnteredDateTime + ", externalLastQuantityDispensed="
				+ externalLastQuantityDispensed + ", rxClosedBy=" + rxClosedBy + "]";
	}
	
	
	
	
}
