import tornadofx.App
import tornadofx.launch
import tornadofx.reloadStylesheetsOnFocus
import views.MasterView
import exception.*
        
class LaunchOmnitool: App(MasterView::class)//, EnabledMenuOptionStyle::class)
{
    init {
        reloadStylesheetsOnFocus()
//        Thread.setDefaultUncaughtExceptionHandler(MyCustomErrorHandler())

    }
}


//Keep this method outside of the above class.
//Kotlin requires a main function outside of a class to launch the app.
fun main(args: Array<String>) {
	if(args.size == 0)
	{
	    launch<LaunchOmnitool>(args)
//        launch(args)
	}
	else
	{
	    //start command line interface here
	    println("I have arguments")
    }
}
