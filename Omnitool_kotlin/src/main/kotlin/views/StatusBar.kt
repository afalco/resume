package views

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.paint.Color
import tornadofx.TaskStatus
import tornadofx.View
import tornadofx.action
import tornadofx.borderpane
import tornadofx.box
import tornadofx.button
import tornadofx.hbox
import tornadofx.label
import tornadofx.style
import tornadofx.useMaxWidth
        
class StatusBar: View() {
    
    //Use this in order to use progress bar over time.
    val status: TaskStatus by inject()
    
//    var statusMessage = SimpleStringProperty("No message")
    
    var statusMessageLabel = Label("No message")
    
    override val root = borderpane()
    {
        center = hbox(10) {
            useMaxWidth = true
            
            alignment = Pos.BASELINE_LEFT
            
            button("Clear status"){}.action {
                clearStatusBarMessage()
            }
            
//            label(statusMessage){
//	            useMaxWidth = true
//	            alignment = Pos.BASELINE_LEFT
		        
//		        style {
//		            borderColor += box(Color.DARKGREY)
//		        }
//		    }
            add(statusMessageLabel)
            
			style {
				borderColor += box(Color.DARKGREY)
			}
	    }
    }
    
    
    fun updateStatusBarMessage(message : String)
    {
        statusMessageLabel.setText(message)
        statusMessageLabel.textFill=Color.BLACK
    }
    
    
    fun updateStatusBarMessage_error(message : String)
    {
        statusMessageLabel.setText("ERROR: " + message)
        statusMessageLabel.textFill=Color.RED
    }
    
    
    fun clearStatusBarMessage()
    {
        statusMessageLabel.setText("")
        statusMessageLabel.textFill=Color.BLACK
    }
    
    
    
}
