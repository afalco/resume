#!/bin/bash

echo
echo Pushing to remote Bitbucket repo...
echo
# Move up a level to root directory to capture ALL changes in the repo
cd ..
git push
echo Git push complete.
echo
exit 0