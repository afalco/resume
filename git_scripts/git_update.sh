#!/bin/bash

MYUPDATETIME=`date +%F.%T`

# echo $MYUPDATETIME
echo Committing to local git repo...
echo
echo "Enter specific description for commit: "
read DESC
echo 
# Move up a level to root directory to capture ALL changes in the repo
cd ..
pwd

echo Adding to git...
git add *

echo Committing to git...
echo Comment: $MYUPDATETIME - $USERNAME - $DESC
git commit -m "$MYUPDATETIME - $USERNAME - $DESC"

echo Git commit complete.
echo

exit 0