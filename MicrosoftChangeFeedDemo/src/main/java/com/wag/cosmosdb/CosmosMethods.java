package com.wag.cosmosdb;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.data.cosmos.CosmosClient;
import com.azure.data.cosmos.CosmosContainer;
import com.azure.data.cosmos.CosmosContainerProperties;
import com.azure.data.cosmos.CosmosDatabase;
import com.azure.data.cosmos.IndexingPolicy;
import com.microsoft.azure.cosmosdb.Document;

import reactor.core.scheduler.Schedulers;

public class CosmosMethods {
	
	private final static Logger log = LoggerFactory.getLogger(CosmosMethods.class);

	
	/**
	 * @param container
	 * @param object
	 */
	public static void createItem(CosmosContainer container, Object object)
    {
    	container.createItem(object)
	        .doOnSuccess(cosmosDatabaseResponse -> log.info("Object type: " + object.getClass().toString() + " - Item: " + cosmosDatabaseResponse.item().id()))
	        .doOnError(throwable -> log.error(throwable.getMessage()))
	        .publishOn(Schedulers.elastic())
	        .block();
    }
    
	
	/**
	 * Base createContainer method with partitionKey=/id
	 * @param database
	 * @param containerName
	 */
	public static void createContainer(CosmosDatabase database, String containerName)
    {
		createContainer(database, containerName, "/id");
    }
	
	
    /**
     * @param database
     * @param containerName
     * @param partitionKeyPath
     */
    public static void createContainer(CosmosDatabase database, String containerName, String partitionKeyPath)
    {
    	// Create the container if it does not exist yet
        CosmosContainerProperties containerSettings = new CosmosContainerProperties(containerName, partitionKeyPath);
        log.info("Container created: " + containerName);
        
        IndexingPolicy indexingPolicy = new IndexingPolicy();
        indexingPolicy.automatic(false);
        containerSettings.indexingPolicy(indexingPolicy);
        database.createContainerIfNotExists(containerSettings, 400)
            .doOnSuccess(cosmosContainerResponse -> log.info("Container: " + cosmosContainerResponse.container().id()))
            .doOnError(throwable -> log.error(throwable.getMessage()))
            .publishOn(Schedulers.elastic())
            .block();
        
        log.info("Container created: " + containerName);
    }
    
    
    
    /**
     * @param client
     * @param databaseName
     */
    public static void createDatabase(CosmosClient client, String databaseName)
    {
    	client.createDatabaseIfNotExists(databaseName)
	        .doOnSuccess(cosmosDatabaseResponse -> log.info("Database: " + cosmosDatabaseResponse.database().id()))
	        .doOnError(throwable -> log.error(throwable.getMessage()))
	        .publishOn(Schedulers.elastic())
	        .block();
    }

    
    /**
     * @param database
     */
    public static void dropDatabase(CosmosDatabase database)
    {
    	
    	database.delete().block();
    }
    
    
    
    public static void dropContainer(CosmosDatabase database)
    {
    	
    	database.delete().block();
    }
    
    
    
    public static void deleteItem(CosmosDatabase database)
    {
    	
    	database.delete().block();
    }
    

    public static String getDatabaseLinkNameBased() {
        return String.format("/dbs/%s", Constants.sourceDatabaseName);
    }
    
    public static String getCollectionLinkNameBased() {
        return String.format("/dbs/%s/colls/%s", Constants.sourceDatabaseName, Constants.sourceContainerName);
    }
    

    public static String getDocumentLinkNameBased(Document doc) {
        return String.format("/dbs/%s/colls/%s/docs/%s", Constants.sourceDatabaseName, Constants.sourceContainerName, doc.getId());
    }
    
    public static void heavyWork() {
        // I may do a lot of IO work: e.g., writing to log files
        // a lot of computational work
        // or may do Thread.sleep()

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
        }
    }
}
