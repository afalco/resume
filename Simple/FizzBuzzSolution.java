package com.random;

import org.junit.jupiter.api.Test;

/*
 * Print numbers from 1 - 100
 * When multiple of 3 print Fizz instead of the number
 * When multilpe of 5 print Buzz instead of the number
 * When multiple of both, print FizzBuzz instead.
 */

public class FizzBuzzSolution {

	@Test
	public void fizzBuzzSolution() {
		for (int i = 1; i <= 100; i++)
		{
			String output = fizzBuzz(i);
			print(output);
		}
	}//end main

	
	/**
	 * @param number
	 * @return
	 */
	public static String fizzBuzz(int number)
	{
		if ( number % 3 == 0 && number % 5 == 0 )
		{
			return "FizzBuzz";
		}
		else if ( number % 3 == 0)
		{
			return "Fizz";
		}
		else if ( number % 5 == 0)
		{
			return "Buzz";
		}
		else
		{
			return Integer.toString(number);
		}
	}

	
	/**
	 * @param string
	 */
	public static void print(String string) {
		System.out.println(string);
	}

}
