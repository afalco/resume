package views

//import controllers.CenterViewController
import controllers.MasterController
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TabPane
import tornadofx.View
import tornadofx.tab
import tornadofx.tabpane
import views.tabs.*

class CenterView: View() {
    
    val mc: MasterController by inject()
    override val root = tabpane {
        tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
        
        tab<FileCreatorTab>()
        tab<KpiProcessorTab>()
        
    }//end root
    
}//end center view
