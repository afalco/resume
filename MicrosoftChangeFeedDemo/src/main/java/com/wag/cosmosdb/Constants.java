package com.wag.cosmosdb;

public class Constants {

	
    // Replace MASTER_KEY and HOST with values from your Azure Cosmos DB account.
    // The default values are credentials of the local emulator, which are not used in any production environment.
    // <!--[SuppressMessage("Microsoft.Security", "CS002:SecretInNextLine")]-->
    public static String ONLINE_MASTER_KEY = "Tce4LXliAB5qJe82k47ATqatCCWpxxHl3hbOuxz0UnHS42ACFm1WtaGsaFyEVJ4BF08eiqjFJa2Y4gdBDgCCiQ==";
    public static String ONLINE_HOST = "https://rxr-cosmos-document.documents.azure.com:443/";
    

    public static String LOCAL_MASTER_KEY = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
    public static String LOCAL_HOST = "https://localhost:8081/";
    
    
	public final static String sourceDatabaseName  = "PrescriptionRaw";
    public final static String sourceContainerName = "PrescriptionByKey";

	public final static String leaseDatabaseName  = "LeaseDatabase";
    public final static String leaseContainerName = "LeaseCollection";
    
	public final static String DATABASE_NAME_PrescriptionByCode                                = "PrescriptionByCode";
	public final static String DATABASE_NAME_PrescriptionByExternalDataEnteredBy               = "PrescriptionByExternalDataEnteredBy";
	public final static String DATABASE_NAME_PrescriptionByExternalLastDispenseEnteredDateTime = "PrescriptionByExternalLastDispenseEnteredDateTime";
	

	public final static String CONTAINER_NAME_PrescriptionByCode                                = "ContainerByCode";
	public final static String CONTAINER_NAME_PrescriptionByExternalDataEnteredBy               = "ContainerByExternalDataEnteredBy";
	public final static String CONTAINER_NAME_PrescriptionByExternalLastDispenseEnteredDateTime = "ContainerByExternalLastDispenseEnteredDateTime";
	

    
	
}
